
// LIBRARIES
#include <TimerOne.h>           // Avaiable from http://www.arduino.cc/playground/Code/Timer1
#include "Arduino.h"
#include <digitalWriteFast.h>
#include <AccelStepper.h>
#include <Servo.h>

/////// QUADRATURE ENCODERS ///////

// Quadrature encoder
#define c_LeftEncoderInterruptA 5 // Interrupt 5 = pin 18
#define c_LeftEncoderInterruptB 4 // Interrupt 4 = pin 19
#define c_LeftEncoderPinA 18
#define c_LeftEncoderPinB 19
#define LeftEncoderIsReversed

volatile bool _LeftEncoderASet;
volatile bool _LeftEncoderBSet;
volatile bool _LeftEncoderAPrev; 
volatile bool _LeftEncoderBPrev;
volatile long _LeftEncoderTicks = 0;

int _LeftEncoderLastTick = 0;

unsigned long currentTime;


//////// ADJUSTMENTS //////////////////////////////////////////////////////

// DIMMER
byte dimMax = 110;              // 128 = off
byte dimMin = 60;               // 0 = on
int dimBulbAngleMin = 0;    // 325/-315 = touching wall
int dimBulbAngleMax = 340;

// STEPPER

int stepStartDeg = 8;                // 40
float stepMaxDeg = 32;         //32/1600/700 20cm from wall // 32/1500/800 hits wall /32/1600/750 breaks
float stepDegIncrease = 1;              // 

int stepSweepWaitStart = 1600; // 1595
int stepSweepWaitEnd = 1600; 

unsigned long stepSpeedStart = 600;
unsigned long stepSpeedEnd = 1500; //  

long stepAccelerationStart = 100;
long stepAccelerationEnd = 750; // / 


//////////////////////////////////////////////////////////////////////////



//////// AC DIMMER ////////

byte acInterrupt = 0;  // Interrupt 0 = Pin 2
int AC_pin1 = 8;     // Output to Opto Triac
float dim1 = 128;     // Dimming level (0-128)  0 = on, 128 = 0ff
int dimAngle = 0;            
volatile int i = 0;               // Variable to use as a counter            
volatile boolean zero_cross = 0;  // Boolean to store a "switch" to tell us if we have crossed zero
int freqStep = 65;                // This is the delay-per-brightness step in microseconds.


/////// STEPPER MOTOR ////////

AccelStepper stepper(1, 3, 4); // 1st=that I use a driver, 2nd = Step, 3rd = Dir
int stepEnable = 11; // enable Pin HIGH while not stepping, to not use current
int stepPos = 0;
float stepDeg = 0; 
unsigned long stepSweepTime = 0;
int stepSweepWait = 0;
boolean positiveSwing = true;
long stepSpeed = 0;
long stepAcceleration = 0;


/////// FUNCTIONS ////////

// AC DIMMER FUNCTIONS //

void zero_cross_detect() {    
  zero_cross = true;               // set the boolean to true to tell our dimming function that a zero cross has occured
  i=0;
  digitalWrite(AC_pin1, LOW);       // turn off TRIAC (and AC)
}  

void dim_check() {                   
  if(zero_cross == true) {      
    if(i>=dim1) {                     
      digitalWrite(AC_pin1, HIGH); // turn on light
    }  
    i++; // increment time step counter                                                        
  }                                  
}   

// ENCODER FUNCTIONS //

// Interrupt service routines for the left motor's quadrature encoder
void HandleLeftMotorInterruptA(){
  _LeftEncoderBSet = digitalReadFast(c_LeftEncoderPinB);
  _LeftEncoderASet = digitalReadFast(c_LeftEncoderPinA);  
  _LeftEncoderTicks+=ParseEncoderLeft();
  _LeftEncoderAPrev = _LeftEncoderASet;
  _LeftEncoderBPrev = _LeftEncoderBSet;
}

void HandleLeftMotorInterruptB(){
  _LeftEncoderBSet = digitalReadFast(c_LeftEncoderPinB);
  _LeftEncoderASet = digitalReadFast(c_LeftEncoderPinA);
  _LeftEncoderTicks+=ParseEncoderLeft();
  _LeftEncoderAPrev = _LeftEncoderASet;
  _LeftEncoderBPrev = _LeftEncoderBSet;
}

int ParseEncoderLeft(){
  if(_LeftEncoderAPrev && _LeftEncoderBPrev){
    if(!_LeftEncoderASet && _LeftEncoderBSet) return 1;
    if(_LeftEncoderASet && !_LeftEncoderBSet) return -1;
  }else if(!_LeftEncoderAPrev && _LeftEncoderBPrev){
    if(!_LeftEncoderASet && !_LeftEncoderBSet) return 1;
    if(_LeftEncoderASet && _LeftEncoderBSet) return -1;
  }else if(!_LeftEncoderAPrev && !_LeftEncoderBPrev){
    if(_LeftEncoderASet && !_LeftEncoderBSet) return 1;
    if(!_LeftEncoderASet && _LeftEncoderBSet) return -1;
  }else if(_LeftEncoderAPrev && !_LeftEncoderBPrev){
    if(_LeftEncoderASet && _LeftEncoderBSet) return 1;
    if(!_LeftEncoderASet && !_LeftEncoderBSet) return -1;
  }
}



/////// SETUP AND LOOP ///////

void setup() {                                      // Begin setup
 
  pinMode(stepEnable, OUTPUT);
  digitalWrite(stepEnable, HIGH); // turn off stepper
 
 // AC DIMMER 
  pinMode(AC_pin1, OUTPUT);    // Bulb 1            // Set the Triac pin as output
  attachInterrupt(acInterrupt, zero_cross_detect, RISING);   // Attach an Interupt to Pin 2 (interupt 0) for Zero Cross Detection
  Timer1.initialize(freqStep);                      // Initialize TimerOne library for the freq we need 
  Timer1.attachInterrupt(dim_check, freqStep); 

 // QUADRATURE ENCODER
  pinMode(c_LeftEncoderPinA, INPUT);      // sets pin A as input
  digitalWrite(c_LeftEncoderPinA, LOW);  // turn on pullup resistors
  pinMode(c_LeftEncoderPinB, INPUT);      // sets pin B as input
  digitalWrite(c_LeftEncoderPinB, LOW);  // turn on pullup resistors 
  attachInterrupt(c_LeftEncoderInterruptA, HandleLeftMotorInterruptA, CHANGE);
  attachInterrupt(c_LeftEncoderInterruptB, HandleLeftMotorInterruptB, CHANGE);

// STEPPER MOTOR

  stepDeg = stepStartDeg;
  delay(3000);       // wait 3s to start
  digitalWrite(stepEnable, LOW); // turn on stepper
} 


void loop() {    
  
  currentTime = millis(); 
  
  dimAngle = _LeftEncoderTicks; 
  
  
  if(dimAngle < 0) {
    dimAngle *= -1;
  }
  dim1 = map(dimAngle, dimBulbAngleMin, dimBulbAngleMax, dimMin, dimMax); 
  
   stepSweepWait = map(stepDeg, stepStartDeg, stepMaxDeg, stepSweepWaitStart, stepSweepWaitEnd); 
   stepSpeed = map(stepDeg, stepStartDeg, stepMaxDeg, stepSpeedStart, stepSpeedEnd);
   stepper.setMaxSpeed(stepSpeed);
   stepAcceleration = map(stepDeg, stepStartDeg, stepMaxDeg, stepAccelerationStart, stepAccelerationEnd);
   stepper.setAcceleration(stepAcceleration);
   
   //if (stepper.distanceToGo() == 0) {  
      if(currentTime > stepSweepTime) {   // perform sweep
          
          if(stepDeg < stepMaxDeg - stepDegIncrease) {
            stepDeg += stepDegIncrease;
          } else {
            stepDeg = stepMaxDeg;
          } 
          stepPos = map(stepDeg, 0, 360, 0, 1600); // 200 steps / 1600 microsteps per revolution
          
          if(positiveSwing) {
            stepPos = -stepPos;
            positiveSwing = false;
          } else {
            positiveSwing = true;
          }
          
          stepper.moveTo(stepPos);
          stepSweepTime = currentTime + stepSweepWait;
      } 
   //}
  stepper.run();
  
} // END LOOP








